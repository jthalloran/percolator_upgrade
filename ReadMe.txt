This repo houses Percolator with an upgraded SVM solver (TRON) 
which significantly improves runtime performance on large-scale 
proteomics data.

The source code is hosted on bitbucket at:
https://bitbucket.org/jthalloran/percolator_upgrade

Percolator usage remains the same, save for the new parameter dictating the number of 
threads to use during SVM learning: -nr NUMTHREADS

John Halloran

/////////////////////////////////////////////
Readme from the original Percolator codebase:
/////////////////////////////////////////////
This repository holds the source code for percolator and format
converters software packages; a software for postprocessing of shotgun
proteomics data.

The source code is hosted at github, binary downloads can be found in the release repository:
https://github.com/percolator/percolator/releases

The building procedure is cmake-based. Helper scripts for building on
different platforms can be found in: admin/builders/

The cleanest way to build is to use the vagrant virtual machine
system, which can be setup using the script: admin/vagrant/manager.sh

Lukas Käll
/////////////////////////////////////////////
/////////////////////////////////////////////
